// JavaScript Document
import {getShips} from './create-ships-on-field.js'; 
import {createField, getMasterArray, getFieldCells, removeFieldCells} from './create-sea.js'; 
 
let next_shot_field = []; //временный массив для заполнения координатами 
							//для следующего хода компьютером, если он попал в предыдущий ход

//события выстрелов
export let shotEvents = {
	//регистрирует события click для каждой ячейки игрового поля - компъютера
	create_click: function() {
		getFieldCells.all_cell()['player'].forEach(function(item_coord, i, arr) {
			jQuery('div#field_for_computer div#'+item_coord).on('click', function(event){
				event.preventDefault();
				event.stopImmediatePropagation();
				
				jQuery('div#field_for_computer button').detach();
				jQuery('div#field_for_computer div.cell').removeAttr('style');

				let button  = jQuery(document.createElement('button'));
				button.attr('class','btn btn-danger m-2 '+item_coord);
				button.css({display: 'block', position: 'absolute', top: (+event.clientY - 180)+'px', left: (+event.clientX - 100)+'px', zIndex: 100});
				button.html('Подтвердить выстрел по полю <h3>'+item_coord+'</h3>');
				jQuery('div#field_for_computer').append(button);

				jQuery('div#field_for_computer div#'+item_coord).attr('style', 'background-color:#FF000033');

				//регистрирует отмену со кнопки подтверждения выстрела
				//если нажать за пределами поля
				thisFn.cancel_ev_click();
				//регистрирует событие для кнопки подтвердить выстрел
				thisFn.applay_click(item_coord);
			}); 
		}); 	
	},
	//тадличка - ваш выстрел, функция включает ее и отключает через пол секунды
	your_short: function() {
		jQuery('div#player_short_field').removeClass('d-none');
		setTimeout(function(){
			jQuery('div#player_short_field').addClass('d-none');
		}, 500);
	}
}

let thisFn = {
	//событе для отмены выстрела, если нажать за пределами поля
	cancel_ev_click: function() {
		jQuery('body').on('click', function(event){
			jQuery('div#field_for_computer button').detach();
			jQuery('div#field_for_computer div.cell').removeAttr('style');
			jQuery('div#field_for_computer button').unbind('click');
		});
	},
	//функция поддтверждения выстрела игрока
	applay_click: function(item_coord) {
		jQuery('div#field_for_computer button.'+item_coord).on('click', function(event){
			event.preventDefault();
			event.stopImmediatePropagation();
			//после подтверждения удаляем кнопку - подтвердить
			jQuery('div#field_for_computer button.'+item_coord).detach();
			//стираем стиль активности с ячеек поля
			jQuery('div#field_for_computer div.cell').removeAttr('style');

			let index_coord_ship = -1;
			let index_arr_coord_ship = false;
			let is_coord;
			//если есть массив с кораблями компьютера
			if (!!getShips.arr_ships()['computer']) {
				//перебераем массив кораблей компьютера
				getShips.arr_ships()['computer'].forEach(function(item_ship, i, arr) {
					//на каждом корабле запрашиваем индекс координаты корабля 
					//исходя их координаты по которой стрляли
					let index = arr[i]['cell'].findIndex(item => item === item_coord);
					is_coord = item_coord;
					//если индекс есть в списке координат корабля
					//значит попал
					if (+index >= 0) {
						//меняем статус ячейки корабля
						//чтобы он был раненым
						arr[i]['status_cell'][item_coord] = 0;
						index_coord_ship = index;
						index_arr_coord_ship = i;
					}
				});
				//удаляем координату выстреля из общего списка, чтобы не стрелять по ней еще раз
				removeFieldCells.all_cell('player', item_coord);
				//меняем счет ходов игрока
				jQuery('div#dashboard_player').html('Количество сделанных ходов: '+(100 - getFieldCells.all_cell()['player'].length));
				//если выстрел попал по кораблю
				if (+index_coord_ship >= 0) {
					//меняем стиль ячейки
					jQuery('div#field_for_computer div#'+item_coord).html('&ndash;');
					jQuery('div#field_for_computer div#'+item_coord).removeClass('brush_ship');
					jQuery('div#field_for_computer div#'+item_coord).removeClass('computer');
					jQuery('div#field_for_computer div#'+item_coord).addClass('injured_ship');
					//удаляем событие клика по ней чтобы не нажимать еще раз
					jQuery('div#field_for_computer #'+item_coord).unbind('click');

					//проверка статуса корабля (убил или ранил) и игры (выиграл или нет)
					let res_status = thisFn.check_status_ship_and_game(is_coord, 'computer');
					if (!!res_status.ship || !!res_status.game) {
						//показать сообщение о новом ходе игрока
						shotEvents.your_short();	
					}else{
						//конец игры - игрок победил, т.к. он ходил последний и попал по последнему кораблю
						createField.load_field_finish('player', (100 - getFieldCells.all_cell()['player'].length));
					}
				}else{
					//если игрок не попал по кораблю
					//меняем стиль ячейки
					jQuery('div#field_for_computer div#'+item_coord).html('&bull;');
					jQuery('div#field_for_computer div#'+item_coord).removeClass('brush_ship');
					jQuery('div#field_for_computer div#'+item_coord).removeClass('computer');
					jQuery('div#field_for_computer div#'+item_coord).addClass('missed');
					jQuery('div#field_for_computer .cell').unbind('click');

					//выполнить ход компьютера
					thisFn.event_short_computer();
				}
			}
		}); 	
	},

	//проверка статуса корабля, т.е. все палубы подбиты или нет, если все
	//то сразу проверяется - всели крабли подбиты, чтобы узнать кончилась игра или нет
	check_status_ship_and_game: function(item_coord, player) {
		if (!!getShips.arr_ships()[player]) {
			let status_ship = 0;
			let status_game = 0;
			//перебераем корабли
			getShips.arr_ships()[player].forEach(function(item_arr_ship, i, arr_ships) {
				let index;
				//перебераем каждую палубу
				arr_ships[i]['cell'].forEach(function(item_ship, i_ship, arr_ship) {
					index = arr_ship.findIndex(item => item === item_coord);
					if (+index >= 0) {
						arr_ships[i]['status_cell'][index] = 0;
						//перебираем статусы каждой палубы
						arr_ships[i]['status_cell'].forEach(function(item_status, i_status, arr_status) {
							if (!!item_status) {
								status_ship = 1;
							}
						});
						//если статус корабля убит меняем стиль на черный - у каждой палубы корабля
						if (!!!status_ship) {
							//стереть соседей текущей ячейки
							nextShot.reset_arr_field();
							arr_ships[i]['cell'].forEach(function(item, i, arr) {
								jQuery('div#field_for_'+player+' div#'+item).addClass('killed_ship');
								jQuery('div#field_for_'+player+' div#'+item).removeClass('injured_ship');
							});
						}
					}
				});
			});
			//если статус корабля текущего корабля оказался - убит
			//запускаем новую проверку кораблей, т.к. может это был последний корабль
			//и может пора заканчивать игру передав статус игры
			if (!!!status_ship) {
				getShips.arr_ships()[player].forEach(function(item_arr_ship, i, arr_ships) {
					arr_ships[i]['status_cell'].forEach(function(item_status, i_status, arr_status) {
						if (!!item_status) {
							status_game = 1;
						}
					});
				});				
			}
			//результаты проверок
			return {ship: +status_ship, game: +status_game};

		}else{
			return false;
		}
	},
	//функция хода копьютера
	event_short_computer: function(){
		//включаем табличку - ходит противник, т.е. компьютер
		jQuery('div#computer_short_field').removeClass('d-none');
		setTimeout(function(){
			jQuery('div#computer_short_field').addClass('d-none');
			let rand_item;
			let coord_player;
console.log(nextShot.get_arr_field());
			//если массив с времеными данными координат заполнен
			//данными соседей, исходя из предыдущего хода
			//то случайно, выбираемкоординату из этого массива
			if (!!nextShot.get_arr_field()[0]) {
				rand_item = Math.floor(Math.random() * Math.floor((+nextShot.get_arr_field().length-1)));
				coord_player = nextShot.get_arr_field()[rand_item];
			}else{
				//если координат соседей нет
				//то случайно выбираем координату из общего массива
				//где находятся координаты всех полей
				//за исключение тех по которым уже стреляли, чтобы не стрелять дважды
				rand_item = Math.floor(Math.random() * Math.floor((+getFieldCells.all_cell()['computer'].length - 1)));
				coord_player = getFieldCells.all_cell()['computer'][+rand_item];
			}
console.log(coord_player);
			let index_coord_ship = -1;
			let index_arr_coord_ship = false;
			let is_coord;
			//если массив кораблей игрока существует
			if (!!getShips.arr_ships()['player']) {
				//перебераем его с целью поиска рандомной координаты
				//которую определил компьютера
				getShips.arr_ships()['player'].forEach(function(item_ship, i, arr) {
					//првоверяем есть ли индекс каждой ячейки кораблей
					let index = arr[i]['cell'].findIndex(item => item === coord_player);
					is_coord = coord_player;
					//если индекс обнаружен, считаем что компьютер попал по кораблю
					if (+index >= 0) {
						index_coord_ship = index;
						index_arr_coord_ship = i;
					} 
				});
				//вычеркиваем текущую координату изобщего массива, 
				//чтобы не стрелять по ней дважды
				removeFieldCells.all_cell('computer', coord_player);
				//меняем количество ходов компьютером
				jQuery('div#dashboard_computer').html('Количество сделанных ходов: '+(100 - getFieldCells.all_cell()['computer'].length));
				//если компьютер поал
				if (+index_coord_ship >= 0) {
					//меняем стили ячейки
					jQuery('div#field_for_player div#'+is_coord).html('&ndash;');
					jQuery('div#field_for_player div#'+is_coord).removeClass('brush_ship');
					jQuery('div#field_for_player div#'+is_coord).removeClass('player');
					jQuery('div#field_for_player div#'+is_coord).addClass('injured_ship');
	
					//проверка статуса корабля (убил или ранил) и игры (выиграл или нет)
					let res_status = thisFn.check_status_ship_and_game(is_coord, 'player');
					if (!!res_status.ship || !!res_status.game) {
						//если корабль еще не убит полностью
						//заполняем массив соседей текущей ячейки
						//чтобы компьютер стрелял поним а не по общему массиву
						if (!!res_status.ship){
							//стереть соседей текущей ячейки
							nextShot.reset_arr_field();
							//заполняем
							nextShot.add_arr_field(coord_player);							
						}
						//запустить повторный ход компьютера
						thisFn.event_short_computer();
					}else{
						//таличка конец игры - выиграл компьютер, 
						//т.к его ход последний и кораблей больше нет
						createField.load_field_finish('computer', (100 - getFieldCells.all_cell()['computer'].length));
					}
				}else{ 
					//если компьютер промахнулся, то меняем стили ячейки
					jQuery('div#field_for_player div#'+is_coord).html('&bull;');
					jQuery('div#field_for_player div#'+is_coord).removeClass('brush_ship');
					jQuery('div#field_for_player div#'+is_coord).removeClass('player');
					jQuery('div#field_for_player div#'+is_coord).addClass('missed');
					//запускаем регистратор событий для каждой ячейки
					//т.к. после промоха игроком, все события отменяются
					//чтобы он не мог ходить, пока не походит компьютер
					shotEvents.create_click();
					//табличка - ход игрока
					shotEvents.your_short();
					//стереть соседей текущей ячейки
					nextShot.reset_arr_field();
				}
			}
		}, 2000);
	}
}

//функции расчета и заполнения массива для следующего выстрела компьютером, 
//если он попал в предыдущий ход
let nextShot = {
	//получить массив
	get_arr_field: function() {
		return next_shot_field;
	},
	//сбросить массив
	reset_arr_field: function() {
		return next_shot_field = [];
	},
	//добавить елементы исходя из полученой координаты
	//добавляет соседа слева, справа, сверх и снизу, если нет края поля
	//эти данные будут использоваться если компьютер будет срелять два раза подряд
	//после второго раза определяются, кординаты повторно
	//путем повторного запуска этой функции
	add_arr_field: function(item_coord) {
		let coord = item_coord.split('');
		let col = coord[0];
		let row = (!!coord[2]) ? coord[1]+coord[2] : coord[1];

		let ind_col = getMasterArray.title_column().findIndex(item => item === col);
		let ind_row = getMasterArray.title_row().findIndex(item => item == +row);
		let res_ind;
		let res_ind1;
		let res_ind2;

		if (ind_col === 'a') {
			res_ind = getFieldCells.all_cell()['computer'].findIndex(item => item === (getMasterArray.title_column()[+ind_col + 1]+getMasterArray.title_row()[ind_row]) );
			if (+res_ind >= 0) {
				next_shot_field.push((getMasterArray.title_column()[+ind_col + 1]+getMasterArray.title_row()[ind_row]));
			}
		}else{
			if (ind_col === 'j') {
				res_ind = getFieldCells.all_cell()['computer'].findIndex(item => item === (getMasterArray.title_column()[+ind_col - 1]+getMasterArray.title_row()[ind_row]) );
				if (+res_ind >= 0) {
					next_shot_field.push((getMasterArray.title_column()[+ind_col - 1]+getMasterArray.title_row()[ind_row]));
				}
			}else{
				res_ind1 = getFieldCells.all_cell()['computer'].findIndex(item => item === (getMasterArray.title_column()[+ind_col + 1]+getMasterArray.title_row()[ind_row]) );
				res_ind2 = getFieldCells.all_cell()['computer'].findIndex(item => item === (getMasterArray.title_column()[+ind_col - 1]+getMasterArray.title_row()[ind_row]) );	
				if (+res_ind1 >= 0) {
					next_shot_field.push((getMasterArray.title_column()[+ind_col + 1]+getMasterArray.title_row()[ind_row]));
				}
				if (+res_ind2 >= 0) {
					next_shot_field.push((getMasterArray.title_column()[+ind_col - 1]+getMasterArray.title_row()[ind_row]));
				}
			}
		}

		if (+ind_row == 1) {
			res_ind = getFieldCells.all_cell()['computer'].findIndex(item => item === (getMasterArray.title_column()[ind_col]+getMasterArray.title_row()[+ind_row+1]) );
			if (+res_ind >= 0) {
				next_shot_field.push((getMasterArray.title_column()[ind_col]+getMasterArray.title_row()[+ind_row+1]));
			}
		}else{
			if (+ind_row == 10) {								
				res_ind = getFieldCells.all_cell()['computer'].findIndex(item => item === (getMasterArray.title_column()[ind_col]+getMasterArray.title_row()[+ind_row-1]) );
				if (+res_ind >= 0) {
					next_shot_field.push((getMasterArray.title_column()[ind_col]+getMasterArray.title_row()[+ind_row-1]));
				}
			}else{
				res_ind1 = getFieldCells.all_cell()['computer'].findIndex(item => item === (getMasterArray.title_column()[ind_col]+getMasterArray.title_row()[+ind_row+1]) );
				res_ind2 = getFieldCells.all_cell()['computer'].findIndex(item => item === (getMasterArray.title_column()[ind_col]+getMasterArray.title_row()[+ind_row-1]) );	
				if (+res_ind1 >= 0) {
					next_shot_field.push((getMasterArray.title_column()[ind_col]+getMasterArray.title_row()[+ind_row+1]));
				}
				if (+res_ind2 >= 0) {
					next_shot_field.push((getMasterArray.title_column()[ind_col]+getMasterArray.title_row()[+ind_row-1]));
				}
			}
		}

	}
}