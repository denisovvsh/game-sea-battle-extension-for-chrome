// JavaScript Document
import {getMasterArray, getFieldCells, addFieldCells, removeFieldCells} from './create-sea.js'; 

let arr_ships = [];

export let getShips = {
	arr_ships: function(){ 
		return arr_ships;
	}
}

export let createShips = {
	add_ship: function(player, pipes){
//console.log('Игрок - '+player+': строит корабль из '+pipes+' палуб');
		//проверка наличия массивов - заголовков полей, строк и колонок
		if (!!Array.isArray(getMasterArray.title_column()) && 
			!!Array.isArray(getMasterArray.title_row())) {
			//вертикальный или горизонтальный
			let vert_or_hor = thisFn.vert_or_hor();
			//временный массив
			let tmp = [];
			//находим допустимые поля для стройки корабля
			//для этого нужно заполнить временный массив:
			//полным набором ячеек
			//после - определить допустимые поля для корабля, исходя из размеров корабля и его положения
			//после - удалить из временного массива ячейки, которые заняты другими кораблями - они находятся в специальном массиве
			tmp = thisFn.create_tmp_arr_without_occupied_cell(player, vert_or_hor, pipes);

			if (!!tmp[player]) {
				//если временный массив с допустимыми полями, заполнен успешно
				//генерируем данные для построения корабля
				//берем случайную координату из временного массива допустимых полей - это будет первая координата корабля
				//после - исходя из размерности корабля получаем крайнюю координату корабля
				//после - проверяем крайнюю координату, входит ли она в область допустимых полей
				//если первая и крайняя координаты входят в свободную область - имеются в временном массиве,
				//то строим кораблик
				//после - добавляем координаты полей корабля, в массив занятых полей
				//а так же, вносим, туда же, поля, которые вокруг нового корабля, т.к. 
				//корабли должны быть расположены через одну клетку, друг от друга
				thisFn.gen_first_coord_for_ship_and_create_ship(tmp, vert_or_hor, pipes, player);
				
//console.log('Допустимые поля для стройки текущего корабля - массив ниже:');
//console.log(tmp[player]);
			}
			return true;
		}else{
			return false;
		}
	} 
}

let thisFn = {
	gen_first_coord_for_ship_and_create_ship: function(arr, vert_or_hor, pipes, player){
		//из массива допустимых полей
		//получим случайную координату начала построения корабля
		let point_start_to_create_ship = Math.floor(Math.random() * Math.floor((+arr[player].length - 1)));
		let first_coord_for_ship = arr[player][point_start_to_create_ship].split('');
		let col = first_coord_for_ship[0];
		let row = (!!first_coord_for_ship[2]) ? first_coord_for_ship[1]+first_coord_for_ship[2] : first_coord_for_ship[1];
//console.log('Первая координата корабля '+arr[player][point_start_to_create_ship]);

		let index_first_coord = getMasterArray.title_column().findIndex(item => item === col);
		//крайняя координата корабля
		let last_coord_for_ship = (vert_or_hor === 'v') ? col+(+row + (+pipes - 1)) : getMasterArray.title_column()[+index_first_coord + (+pipes - 1)]+row;

		//получаем индекс поля крайней координаты в массиве доступных, для строительства координат
		//тем самым узнаем - войдет ли новый кораблик в свободную область
		let index_last_coord = arr[player].findIndex(item => item === last_coord_for_ship);

		//если корабль 4 палубы, то он строится первым и 
		//в проверке доступной области не нуждается
		//по этому просто строим корабль
		if (+pipes == 4) {
//console.log('Проверка свободной области, для строительства - пройдена!');
			//генерируем кораблик
			thisFn.create_ship(vert_or_hor, pipes, index_first_coord, first_coord_for_ship, arr, player);
			//добавляем ячейки корабля в массив занятых ячеек
			thisFn.insert_occupied_part_cell(vert_or_hor, player, pipes, index_first_coord, row);
		}else{
			//если индекс крайней координаты получен успешно
			if (+index_last_coord >= 0) {
//console.log('Проверка свободной области, для строительства - пройдена!');
				//генерируем кораблик
				thisFn.create_ship(vert_or_hor, pipes, index_first_coord, first_coord_for_ship, arr, player);
				//добавляем ячейки корабля в массив занятых ячеек
				thisFn.insert_occupied_part_cell(vert_or_hor, player, pipes, index_first_coord, row);
			}else{
				//иначе запускаем новый поиск координат для корабля и его строительство
//console.log('Нужна новая координата для стройки корабля!');
				setTimeout(function(){
					thisFn.gen_first_coord_for_ship_and_create_ship(arr, vert_or_hor, pipes, player);
				}, 100);
			}
		}
	},

	create_ship: function(vert_or_hor, pipes, index, first_coord_for_ship, arr_tmp_ships, player){
		//если в массиве кораблей игрока еще нет ни одного корабля
		//создадим пустой массив, куда будут добавляться новые корабли - их координаты
		if (!!!arr_ships[player]) {arr_ships[player] = [];}

		if (!!arr_tmp_ships[player]) {
			//из переданой, первой координаты корабля
			//извлечем название ее колонки и номер строки
			let col = first_coord_for_ship[0];
			let row = (!!first_coord_for_ship[2]) ? first_coord_for_ship[1]+first_coord_for_ship[2] : first_coord_for_ship[1];
			
			//заполним первые параметры корабля
			arr_ships[player].push({pipes:(+pipes), cell:[(col+row)], status_cell:[1]});
			//порядковый индекс объекта - только что созданного, корабля в массиве кораблей
			//нужен, чтобы добавить в него остальные параметры
			let index_ships = +arr_ships[player].length - 1; 
			//заполним остальные параметры корабля
			//если вертикальный
			if (vert_or_hor === 'v') { 
				for (let j = 1; j < (+pipes); j++) { 
					row++;
					//координаты ячеек окрабля
					arr_ships[player][index_ships]['cell'].push(col+row);
					//статус ячейки корабля, понадобится для определения ранен или нет
					//по умолчанию статус живой - не подбит
					arr_ships[player][index_ships]['status_cell'].push(1);
				}
			}else{
				//горизонтальный
				for (let j = 1; j < (+pipes); j++) { 
					index++;
					arr_ships[player][index_ships]['cell'].push(getMasterArray.title_column()[index]+row);
					arr_ships[player][index_ships]['status_cell'].push(1);
				}
			}
			return true;
		}else{
			return false;
		}
	},

	vert_or_hor: function(){
		//определение положения корабля вертикальный или горизонтальный
		//случайное число от 0 до 10
		let vert_or_hor = Math.floor(Math.random() * Math.floor(11));
		//если входит в первые 5, то вертикальный
		if (+vert_or_hor < 6) {
			return 'v';
		}else{
			//иначе горизонтальный
			return 'h';
		}
	},

	create_tmp_arr_without_occupied_cell: function(player, vert_or_hor, pipes){
		//генерация массива с разрешенными строками или полями
		//зависит от положения корабля
		//понядобится для генерации массивы допустимых координат
		let tmp = [];
		//шаг цыкла определяется количеством палуб у корабля
		//чтобы кораблик не строился за пределами игрового поля
		let p = 11 - (+pipes);
		if (vert_or_hor === 'v') {
			//если вертикльный ограничивается шаг, по строкам поля игры
			for (let j = 1; j <= p; j++) { 
				//значения заголовков строк поля игры
				tmp.push(getMasterArray.title_row()[j]);
			}
		}else{
			//иначе по колонкам
			for (let j = 1; j <= p; j++) { 
				//значения заголовков колонок поля игры
				tmp.push(getMasterArray.title_column()[j]);
			}
		}
	
		//добавим объект игорока с массивом допустимых полей - координат
		//для будующего корабля
		let arr_tmp_ships = [];
		arr_tmp_ships[player] = thisFn.create_tmp_player_object_with_permissible_cells(vert_or_hor, pipes, tmp);

		//если в массиве занятых ячеек есть какие либо ячейки
		//удалим из массива допустимых полей эти ячейки
		//чтобы при определении нового корабля не попасть в существующий
		if (!!getFieldCells.occupied()[player] && !!arr_tmp_ships[player]) {

//console.log('Занятые полея другими кораблями - массив ниже:');
//console.log(getFieldCells.occupied()[player]);

			getFieldCells.occupied()[player].forEach(function(item_occ, i, arr) { 
				let index_occ = arr_tmp_ships[player].findIndex(item => item === item_occ);		
				if (+index_occ >= 0) {
					arr_tmp_ships[player].splice(index_occ, 1); 
				}
			});
		}else{
//console.log('Занятых полей не обнаружено - строим где угодно!');
		}

		return arr_tmp_ships;
	},

	create_tmp_player_object_with_permissible_cells: function(vert_or_hor, pipes, tmp){
		//генерируем массив допустимых полей - координат для построения корабля
		let arr = [];
		let p = 10 - (+pipes);
		if (vert_or_hor === 'v') {
			for (let i = 0; i <= p; i++) { 
				for (let j = 1; j <= 10; j++) { 
					arr.push(getMasterArray.title_column()[j]+tmp[i]);
				}
			}
		}else{
			for (let j = 1; j <= 10; j++) { 
				for (let i = 0; i <= p; i++) { 
					arr.push(tmp[i]+getMasterArray.title_row()[j]);
				}
			}
		}
		return arr;
	},

	insert_occupied_part_cell: function(vert_or_hor, player, pipes, index, row){
		//заполняем массив занятых полей координатами корабля и 
		//окружающими его полями
		//исходя из его положения и размерности
		addFieldCells.occupied(player, (getMasterArray.title_column()[index]+row));
		if (vert_or_hor === 'v') {
			//вертикально
			//если корабль прилегает к левой границе игрового поля
			if (getMasterArray.title_column()[index] === 'a') {
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+row));
				//еще часть окружающих полей
				thisFn.insert_occupied_part_cell_v_col_a(vert_or_hor, player, pipes, index, row);
			}else{
				//если корабль прилегает к правой границе игрового поля
				if (getMasterArray.title_column()[index] === 'j') {
					addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+row));
					//еще часть окружающих полей
					thisFn.insert_occupied_part_cell_v_col_j(vert_or_hor, player, pipes, index, row);
				}else{
					//если корабль на расстоянии от границ
					addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+row));
					addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+row));
					//еще часть окружающих полей
					thisFn.insert_occupied_part_cell_v_col_else(vert_or_hor, player, pipes, index, row);
				}	
			}

			//ячейки самого корабля
			for (let j = 1; j < (+pipes); j++) { 
				row++;
				//добавляем ячейки корабля в массив занятых ячеек
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+row));
				if (getMasterArray.title_column()[index] === 'a') {
					addFieldCells.occupied(player, ('b'+row));
				}else{
					if (getMasterArray.title_column()[index] === 'j') {
						addFieldCells.occupied(player, ('i'+row));
					}else{
						addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+row));
						addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+row));
					}	
				}
			}
		}else{
			//горизонтально
			//если корабль прилегает к верхней границе игрового поля
			if (+row == 1) {
				addFieldCells.occupied(player, (getMasterArray.title_column()[index])+(+row+1));
				//еще часть окружающих полей
				thisFn.insert_occupied_part_cell_h_row_1(vert_or_hor, player, pipes, index, row);
			}else{
				//если корабль прилегает к нижней границе игрового поля
				if (+row == 10) {
					addFieldCells.occupied(player, (getMasterArray.title_column()[index])+(+row-1));
					//еще часть окружающих полей
					thisFn.insert_occupied_part_cell_h_row_10(vert_or_hor, player, pipes, index, row);
				}else{
					//если корабль на расстоянии от границ
					addFieldCells.occupied(player, (getMasterArray.title_column()[index])+(+row-1));
					addFieldCells.occupied(player, (getMasterArray.title_column()[index])+(+row+1));
					//еще часть окружающих полей
					thisFn.insert_occupied_part_cell_h_row_else(vert_or_hor, player, pipes, index, row);
				}	
			}

			let index_n = index;
			//ячейки самого корабля
			for (let j = 1; j < (+pipes); j++) { 
				index_n++
				//добавляем ячейки корабля в массив занятых ячеек
				addFieldCells.occupied(player, (getMasterArray.title_column()[index_n]+row));
				if (+row == 1) {
					addFieldCells.occupied(player, getMasterArray.title_column()[index_n]+(+row+1));
				}else{
					if (+row == 10) {
						addFieldCells.occupied(player, getMasterArray.title_column()[index_n]+(+row-1));
					}else{
						addFieldCells.occupied(player, getMasterArray.title_column()[index_n]+(+row-1));
						addFieldCells.occupied(player, getMasterArray.title_column()[index_n]+(+row+1));
					}	
				}
			}
		}
	},

	insert_occupied_part_cell_v_col_a: function(vert_or_hor, player, pipes, index, row){
		if (+row == 1) {
			addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row+(+pipes))));
			addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+(+row+(+pipes))));
		}else{
			if (+row == 10) {
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row-1)));
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+(+row-1)));
			}else{
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row+(+pipes))));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row-1)));

				addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+(+row+(+pipes))));
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+(+row-1)));
			}	
		}
	},

	insert_occupied_part_cell_v_col_j: function(vert_or_hor, player, pipes, index, row){
		if (+row == 1) {
			addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row+(+pipes))));
			addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+(+row+(+pipes))));
		}else{
			if (+row == 10) {
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row-1)));
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+(+row-1)));
			}else{
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row+(+pipes))));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row-1)));

				addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+(+row+(+pipes))));
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+(+row-1)));
			}	
		}
	},

	insert_occupied_part_cell_v_col_else: function(vert_or_hor, player, pipes, index, row){
		if (+row == 1) {
			addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row+(+pipes))));
			addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+(+row+(+pipes))));
			addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+(+row+(+pipes))));
		}else{
			if (+row == 10) {
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row-1)));
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+(+row-1)));
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+(+row-1)));
			}else{
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row+(+pipes))));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index]+(+row-1)));

				addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+(+row+(+pipes))));
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+(+row+(+pipes))));
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index + 1]+(+row-1)));
				addFieldCells.occupied(player, (getMasterArray.title_column()[+index - 1]+(+row-1)));
			}	
		}
	},


	insert_occupied_part_cell_h_row_1: function(vert_or_hor, player, pipes, index, row){
		let x;
		switch(+pipes) {
		  case 4:
		    x = 'g';
		    break;
		  case 3:
		    x = 'h';
		    break;
		  case 2:
		    x = 'i';
		    break;
		  default:
		    x = 'j';
		    break;
		}

		if (getMasterArray.title_column()[index] === 'a') {
			addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(row));
			addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(+row+1));
		}else{
			if (getMasterArray.title_column()[index] === x) {
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(row));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(+row+1));
			}else{
				addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(row));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(row));	

				addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(+row+1));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(+row+1));	
			}
		}
	},

	insert_occupied_part_cell_h_row_10: function(vert_or_hor, player, pipes, index, row){
		let x;
		switch(+pipes) {
		  case 4:
		    x = 'g';
		    break;
		  case 3:
		    x = 'h';
		    break;
		  case 2:
		    x = 'i';
		    break;
		  default:
		    x = 'j';
		    break;
		}

		if (getMasterArray.title_column()[index] === 'a') {
			addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(row));
			addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(+row-1));
		}else{
			if (getMasterArray.title_column()[index] === x) {
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(row));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(+row-1));
			}else{
				addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(row));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(row));	

				addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(+row-1));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(+row-1));	
			}
		}
	},

	insert_occupied_part_cell_h_row_else: function(vert_or_hor, player, pipes, index, row){
		let x;
		switch(+pipes) {
		  case 4:
		    x = 'g';
		    break;
		  case 3:
		    x = 'h';
		    break;
		  case 2:
		    x = 'i';
		    break;
		  default:
		    x = 'j';
		    break;
		}
		
		if (getMasterArray.title_column()[index] === 'a') {
			addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(row));
			addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(+row-1));
			addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(+row+1));
		}else{
			if (getMasterArray.title_column()[index] === x) {
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(row));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(+row-1));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(+row+1));
			}else{
				addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(row));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(row));	

				addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(+row-1));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(+row-1));	

				addFieldCells.occupied(player, (getMasterArray.title_column()[index+(+pipes)])+(+row+1));
				addFieldCells.occupied(player, (getMasterArray.title_column()[index-1])+(+row+1));	
			}
		}
	}
} 