// JavaScript Document
import {createField} from './create-sea.js'; 

if (!!createField.load_field_start()) {
	//рисуем поля для игроков
	createField.add_row_and_cell('player', 'div#field_for_player');
	createField.add_row_and_cell('computer', 'div#field_for_computer'); 	
	setTimeout(function() {
		//запускаем процесс расчета всех кораблей, запонение массивов данными
		createField.start_add_ships();
	}, 100); 
} 