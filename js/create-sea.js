// JavaScript Document
import {createShips, getShips} from './create-ships-on-field.js'; 
import {shotEvents} from './shot-events.js'; 

let title_column = [0, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
let title_row = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

let free_field_cells = [];
let occupied_field_cells = [];

export let createField = {
	//отображает стартовое сообщение, после нажатия кнопки запустить игру
	load_field_start: function(){
		jQuery('div#warning_message_field').toggleClass('d-none');
		jQuery('div#warning_message_field').css({height: jQuery(window).height()+'px'});
		jQuery('div#warning_message_field .alert').html('По полученным развед. данным - в наши территориальные воды, <br>');
		jQuery('div#warning_message_field .alert').append('вторглась неизвестная группировка кораблей.<br>');
		jQuery('div#warning_message_field .alert').append('Командование ВМФ, отдало приказ перебросить оборонительные силы для защиты квадрата.<br>');
		jQuery('div#warning_message_field .alert').append('Ожидаем переброску группировки войск <br>');
		jQuery('div#warning_message_field .alert').append('к месту вооруженного конфликта...<br>');
		loaderFn.inamation_circle('div#warning_message_field .alert', 'block');
		return true;
	},
	//отображает сообщение при финише игры - кто победил, сколько ходов сделал
	load_field_finish: function(player, step){
		jQuery('div#sea_battle_page').toggleClass('d-none');
		jQuery('div#warning_message_field').toggleClass('d-none');
		jQuery('div#warning_message_field').css({height: jQuery(window).height()+'px'});
		jQuery('div#warning_message_field .alert').html('<h3>Конец игры! '+((player === 'computer') ? 'Победил компьютер!' : 'Вы победили!')+'</h3><p>Количество ходов '+step+'</p>');
		//кнопка запустить игру снова
		let button  = jQuery(document.createElement('button'));
		button.attr( 'class','btn btn-primary m-2 float-none');
		button.html('Начать сражение!');
		jQuery('div#warning_message_field .alert').append(button);
		//событие перезагрузки страницы для повторного запуска игры
		jQuery('div#warning_message_field .alert button').on('click', function(event){
			event.preventDefault();
			event.stopImmediatePropagation();
			
			window.location.reload();
		}); 	

		return true;
	},

	start_add_ships: function(pipes=4, a_sum=0, b_sum=0, c_sum=0) {
		let n = (+pipes); //счетчик количества палуб
		let a = (+a_sum); //счетчик количества кораблей - 3 палубы
		let b = (+b_sum); //счетчик количества кораблей - 2 палубы
		let c = (+c_sum); //счетчик количества кораблей - 1 палуба
		let res1; //результат добавления корабля для игрока
		let res2; //результат добавления корабля для компьютера

		//запускается процесс формирования кораблей, для каждого игрока
		switch(+n) {
		  case 4:
		  	n--;
		    res1 = createShips.add_ship('player', pipes);	
			res2 = createShips.add_ship('computer', pipes);	
		    break;
		  case 3:
		  	if (+a < 2) {
				a++;
				res1 = createShips.add_ship('player', pipes);	
				res2 = createShips.add_ship('computer', pipes);	
			}
			if(+a == 2){
				n--;
			}
		    break;
		  case 2:
		  	if (+b < 3) {
				b++;
				res1 = createShips.add_ship('player', pipes);	
				res2 = createShips.add_ship('computer', pipes);	
			}
			if(+b == 3){
				n--;
			}
		    break;
		  default:
		  	if (+c < 4) {
				c++;
				res1 = createShips.add_ship('player', pipes);	
				res2 = createShips.add_ship('computer', pipes);	
			}
			if(+c == 4){
				n--;
			}
		    break;
		} 
		//если формирование партии кораблей прошло успешно
		//а счетчик сигнализирует что нужны еще корабли, 
		//то функция запускается еще раз с новыми параметрами
		//и так пока все корабли не будут построенны в полном комплекте
		if (!!res1 && !!res2) { 
			if (!!+n) {
				setTimeout(function(){
					createField.start_add_ships(n, a, b, c);
				}, 800);
			}else{
				//если все построено, то производится вывод разметки на экран
				jQuery('div#sea_battle_page').toggleClass('d-none');
				createField.print_all();
			}
		}
	},

	print_all: function(){
		//отключаем лоадер
		loaderFn.inamation_circle('div#warning_message_field .alert');
		//добавить кнопку "Старт игры" в вступительное сообщение
		//кнопка скрывает сообщение и включает отображение страницы с полями игры
		createField.create_button_start();

		//закрасить клетки ячеек кораблей, в соответствии с координатами в массиве кораблей игрока
		if (!!getShips.arr_ships()['player']) {
			getShips.arr_ships()['player'].forEach(function(item, i, arr) {
				arr[i]['cell'].forEach(function(item, i, arr) {
					jQuery('div#field_for_player div#'+item).addClass('brush_ship');
					jQuery('div#field_for_player div#'+item).removeClass('player');
				});
			});
		}

		//зарегистрировать событие клик для каждой клетки поля коспьютера
		if (!!getFieldCells.all_cell()['computer']) {
			shotEvents.create_click();
		}
	},
	//отображает кнопку запуска игры, при старте, после згрузки всех компанентов
	create_button_start: function(){
		let button  = jQuery(document.createElement('button'));
		button.attr( 'class','btn btn-primary m-2 float-none');
		button.html('Начать сражение!');
		jQuery('div#warning_message_field .alert').append(button);

		jQuery('div#warning_message_field .alert button').on('click', function(event){
			event.preventDefault();
			event.stopImmediatePropagation();
			jQuery('div#warning_message_field').toggleClass('d-none');
			//табличка - ваш выстрел
			shotEvents.your_short();
		}); 	
	}, 
	//формирует и отображает поля 10х10 для каждого игрока
	add_row_and_cell: function(player, id_element){
		if (!!jQuery("div").is(id_element)) {
			getMasterArray.title_row().forEach(function(item, i, arr) {
				let div  = jQuery(document.createElement('div'));
				div.attr( 'class','row p-0 m-0');
				div.attr('id', 'row_'+item);
				div.append('<div></div>');
				div.children().eq(0).attr( 'class','row_cell text-primary text-center float-left');
				div.children().eq(0).attr('id', 'title_row_'+item);
				div.children().eq(0).html((!!+i) ? item : '&nbsp;');
				
				for (let j = 1; j < 11; j++) { 
					div.append('<div></div>');
					if (!!+i) {
						div.children().eq(j).attr( 'class','cell text-primary text-center float-left');
						div.children().eq(j).attr('id', getMasterArray.title_column()[j]+i);
						div.children().eq(j).addClass(player);
						//каждую созданную ячейку поля добавляем в спец массив
						//чтобы потом вычеркивать сделанные ходы и для того чтобы компьютер 
						//случайно выбирал ячейку, для следующего хода
						addFieldCells.all_cell(player, getMasterArray.title_column()[j]+i);
						
					}else{
						div.children().eq(j).attr( 'class','column_cell text-primary text-center float-left');
						div.children().eq(j).attr('id', 'title_column_'+getMasterArray.title_column()[j]);
						div.children().eq(j).html(getMasterArray.title_column()[j]);		
					}
				}
				jQuery(id_element).append(div);
			});		
		}
		return true; 
	}
}
//функции для получения массивов
export let getFieldCells = {
	all_cell: function(){
		return free_field_cells;
	}, 

	occupied: function(){ 
		return occupied_field_cells;
	}
} 
//для добавления нновых данных в массивы
export let addFieldCells = {
	all_cell: function(player, i){
		if (!!free_field_cells[player]) {
			free_field_cells[player].push(i);
		}else{
			free_field_cells[player] = [];
			free_field_cells[player].push(i);
		}
	}, 

	occupied: function(player, i){ 
		if (!!occupied_field_cells[player]) {
			occupied_field_cells[player].push(i);
		}else{
			occupied_field_cells[player] = [];
			occupied_field_cells[player].push(i);
		}
	}
}
//для удаления данных из массивов
export let removeFieldCells = {
	all_cell: function(player, i){
		let index = free_field_cells[player].findIndex(item => item === i);
		if (+index >= 0) {
			free_field_cells[player].splice(index, 1);
			return true;
		}else{
			return false;
		}
	}, 

	occupied: function(player, i){ 
		let index = occupied_field_cells[player].findIndex(item => item === i);
		if (+index >= 0) {
			occupied_field_cells[player].splice(index, 1);
			return true;
		}else{
			return false;
		}
	}
}
//для получения данных заголовков полей и строк, игрового поля
export let getMasterArray = {
	title_column: function(){
		return title_column;
	}, 

	title_row: function(){ 
		return title_row;
	}
} 
//лоадер - включить и выключить
export let loaderFn = {
	inamation_circle: function(elementId=false, display_status=false){
		if (!!display_status) {
			if (!!!jQuery("div").is("div#warning_message_field div#loader_wrapper")) {
				let loader = jQuery(document.createElement('div'));
				loader.attr('id', 'loader_wrapper');
				loader.prepend('<div id="loader_circle1"></div>');
				loader.prepend('<div id="loader_circle2"></div>');
				loader.prepend('<div id="loader_circle3"></div>');
				loader.prepend('<div id="loader_circle4"></div>');
				jQuery( elementId ).append( loader );
			}
		}else{
			if (!!jQuery("div").is("div#warning_message_field div#loader_wrapper")) {
				jQuery( '#loader_wrapper' ).remove();
			}
		}
	}
}