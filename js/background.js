// JavaScript Document
// Вызывается, когда пользователь нажимает на действие браузера.
chrome.browserAction.onClicked.addListener(function(tabs) {
  //chrome.tabs.create({"url": "index.html"}, function(tabs) { });
});


chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if( request.message === "open_new_tab" ) {
      chrome.tabs.create({"url": request.url});
    }
  }
);